# Instruction

Description of file

* term.cmm. This file is used to set terminal. Attention, this script work only for MS Windows platforms
* start_kernel.cmm. This file load binaries into RAM, debug symbol and change the path for source file. Finally there are the instruction to arrived to "Start Kernel ..." string
* breakpoint_file.cmm. This file set the breakpoint to execute
* init_windows.cmm. Open common windows for debug
* gpio_debug.cmm. This file must be perform for start debug

# Work Flow

Before to start to work, please add the following changes.

* Define COM port on `term.cmm`
* Apply the own custom parameters on `start_kernel.cmm`.
* Define the brakpoint to perform in `breakpoint_file.cmm`

Finally perform `gpio_debug.cmm` script.
