; --------------------------------------------------------------------------------
; @Title: Simple demo script for IMX8MM on X-8MMINILPD4-EVK (RAM)
; @Description:
;   Loads the sieve demo application into RAM and sets up a demo debug
;   scenario.
;   Use this script for getting started.
;   Prerequisites:
;    * Connect Debug Cable/Combiprobe to J902 using Adapter LA-3770
;    * set bootmode to SD-Card and remove SD-Card -> A53_0 will stop in BootROM
;      set SW1101[1..8]=0y01000110 SW1102[1..8]=0y00110100
; @Keywords: ARM, Cortex-A53
; @Author: BES
; @Board: X-8MMINILPD4-EVK
; @Chip: IMX8MMINIQUAD
; @Copyright: (C) 1989-2022 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: x-8mminilpd4-evk_sieve_sram.cmm 20295 2023-01-13 13:42:00Z bschroefel $


;WinCLEAR

; --------------------------------------------------------------------------------
; check prerequisites
IF VERSION.BUILD.BASE()<116592. ; 2020-01-22
(
  PRINT %ERROR "Please use more recent Software! Contact support@lauterbach.com."
  ENDDO
)

; --------------------------------------------------------------------------------
; initialize and start the debugger
RESet
SYStem.RESet
SYStem.CPU IMX8MM
SYStem.JtagClock 10MHz
SYStem.Option ResBreak OFF
SYStem.Option WaitIDCODE 1.5s
CORE.ASSIGN 1.
Trace.DISable
SYStem.Mode.Prepare

; --------------------------------------------------------------------------------
; Reset first core an catch at the reset vector
DO kick-cores "A53_0"

SYStem.Mode.Attach

List

;Data.load.Binary "Y:\u-boot-imx\spl\u-boot-spl.bin" 0x7e1000

; --------------------------------------------------------------------------------
; load demo program (uses internal RAM only)

;Data.LOAD.Elf "Y:\sieve_ram_aarch64_v8.elf"
;Go.direct main
;WAIT !STATE.RUN()
Data.load.Binary "Y:\u-boot-imx\spl\u-boot-spl.bin" 0x00904000 /NoClear
Data.load.Elf "Y:\u-boot-imx\spl\u-boot-spl" /NoClear

sYmbol.SourcePATH.Translate "\home\dooraim\temp\test_samba\sdv18-mkimage" "Y:\"
sYmbol.SourceLOAD
